let workspace = document.getElementById("workspace");
let button1 = document.getElementById("button1");
let button2 = document.getElementById("button2");


function createNewPara(newText) {

	let para = document.createElement("p");
	let node = document.createTextNode(newText);
	para.appendChild(node);
	let element = document.getElementById("workspace");
	element.appendChild(para);
	
}

function isInteger(x) {
	return x % 1 === 0;
}

function fizzBuzz() {
	for (i = 1; i<101; i++) {
		switch(true) {
			case (isInteger((i/15))):
			 createNewPara("FizzBuzz");
			 break;

			case (isInteger((i/5))):
			 createNewPara("Buzz");
			 break;

			case (isInteger((i/3))):
			 createNewPara("Fizz");
			 break;

			default:
			 createNewPara(i);
			 break;
		}
	}
}




button1.addEventListener("click", fizzBuzz);
button2.addEventListener("click", changeColor2);
