let workspace = document.getElementById("workspace");
let button1 = document.getElementById("button1");
let button2 = document.getElementById("button2");


function createNewPara(newText) {

	let para = document.createElement("p");
	let node = document.createTextNode(newText);
	para.appendChild(node);
	let element = document.getElementById("workspace");
	element.appendChild(para);
	
}

function isInteger(x) {
	return x % 1 === 0;
}

function fizzBuzz() {
	for (i = 1; i<101; i++) {
		if (isInteger((i/15)) == true) {
			createNewPara("FizzBuzz")
			}
		else  if (isInteger((i/5)) == true) {
			createNewPara("Buzz")
			}
		else if (isInteger((i/3)) == true) {
			createNewPara("Fizz")
			}
		else {
			createNewPara(i)
			}		
	}
}




button1.addEventListener("click", fizzBuzz);
button2.addEventListener("click", changeColor2);
